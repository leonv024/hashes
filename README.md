![alt text](./hashes.png)

# hashes.py
Check if a file hash matches with the file you've got

Usage:

Windows:
```Shell
py hashes.py
```

Linux:
```Shell
python3 hashes.py
```
